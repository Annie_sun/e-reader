/**
 * COMP3331 Ass 1 (E-reader + server) 
 * @author Annie Sun (z5075255)
 */

import java.util.ArrayList;
import java.io.Serializable;

public class PushUser implements Serializable {

	/**
	 * Creates a serializable PushUser object
	 * @param port port   number that the reader is connected to
	 * @param hostName    hostname of the reader
	 * @param userName    name of the reader
	 * @param pushedPosts list of posts that have been sent to the push mode reader
	 */
	public PushUser (int port, String hostName, String userName, ArrayList<Integer> pushedPosts) {
		this.user = userName;
		this.hostName = hostName;
		this.port = port;
		this.pushedPosts = pushedPosts;

	}

	/**
	 * Return port number as an int
	 * @return port number 
	 */
	public int getPortNum() {
		return this.port;
	}

	/**
	 * Returns the username of the reader as a string
	 * @return username of push mode reader
	 */
	public String getPushUser() {
		return this.user;
	}

	/**
	 * Returns an array of integers 
	 * @return serial nums of posts that have been pushed
	 */
	public ArrayList<Integer> getPostIDs(){
		return this.pushedPosts;
	}

	/**
	 * Returns host name as a string
	 * @return host name
	 */
	public String getHostName() {
		return this.hostName;
	}

	/** 
	 * @Override
	 * Converts PushUser object into a string representation
	 */
	public String toString() {
		return "PushUser{" + "port=" + port + "hostName" + hostName + ", user" + user + ", pushedPosts" + pushedPosts + '}';
	}

	private String user;
	private int port;	
	private String hostName;
	private ArrayList<Integer> pushedPosts;
}

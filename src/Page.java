/**
 * COMP3331 Ass 1 (E-reader + server) 
 * @author Annie Sun (z5075255)
 */

import java.util.ArrayList;
import java.io.Serializable;

public class Page implements Serializable {
	
	/**
	 * Creates a serializable Page object 
	 * @param book name of book
	 * @param page page number 
	 */
	
	public Page(String book, int page) {
		this.bookName = book;
		this.pageNum = page;	
	}
	
	/**
	 * Returns the page number as an int
	 * @return page number of this particular page
	 */
	public int getPageNum(){
		return this.pageNum;
	}
	
	/**
	 * Returns the book name as a string
	 * @return book name of this particular page
	 */
	public String getBookName(){
		return this.bookName;
	}
	
	private int pageNum;
	private String bookName;	
	public ArrayList<String> lines = new ArrayList<String>();

	/** 
	 * @Override
	 * Converts Page object into a string representation
	 */
	  public String toString() {
	    return "Display{" + "bookName='" + bookName + '\'' + ", pageNum=" + pageNum + 
	    		", lines=" + lines + '}';
	}	
}

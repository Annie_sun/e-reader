/**
 * COMP3331 Ass 1 (E-reader + server) 
 * @author Annie Sun (z5075255)
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Reader {

	/**
	 * Main method of Reader
	 * Creates a reader object with the following inputs: <br>
	 * mode, polling_interval, user_name, server_name, server_port_number
	 * @param args  inputs as stated above ^
	 * @throws Exception 
	 */
	public static void main (String[] args) throws Exception {
		if (args.length == VALID_INPUT) {
			Reader r = new Reader(args);	
		} 		
	}

	
	public Reader (String[] args) throws Exception {

		// mode polling_interval user_name server_name server_port_number
		mode = args[MODE];
		pollingInterval = Integer.parseInt(args[POLLING_INTERVAL]);
		// change it to milliseconds by multiplying by 1000 as 1000 milliseconds = 1 second
		pollingInterval = pollingInterval * 1000;	
		userName = args[USER_NAME];
		serverName = args[SERVER_NAME];
		serverPortNum = Integer.parseInt(args[SERVER_PORT_NUM]);

		serverAddress = InetAddress.getByName(serverName);
		
		// creates a TCP connection with server for PUSH MODE ONLY 
		if (mode.equals("push")){
			int portNum = randomPortNumber();
			boolean success = false;
			
			// keep trying until there is an empty socket
			while (!success) {
				pushSocket = new ServerSocket(portNum); 
				if (pushSocket != null) {
					success = true;			
					registerPushUser(portNum, userName) ;
				}
			}
		}

		Scanner scanner = new Scanner(System.in);
		while (scanner.hasNext()) {
			String current = scanner.nextLine();
			int firstSpace = current.indexOf(" ");

			String command = current.substring(0, firstSpace);
			String[] input = current.substring(firstSpace + 1).split(" ", 2);

			// format: display book_name page_number
			if (command.equals("display")) {
				String book = input[BOOK_NAME];
				int page = Integer.parseInt(input[PAGE_NUM]);
				display(book, page); 

			// format: post_to_forum line_number content_of_post
			} else if (command.equals("post_to_forum")) {
				int lineNum = Integer.parseInt(input[LINE_NUM]);
				String content = input[CONTENT];
				post_to_forum(lineNum, content);

			// format: read_post line_number
			} else if (command.equals("read_post")) {
				int lineNum = Integer.parseInt(input[LINE_NUM]);
				read_post(lineNum);

			} else {
				System.out.println("Invalid command");
			}		
		}
		scanner.close();
	}

	/**
	 * Method that executes display <book> <page> command 
	 * @param book  name of book to display
	 * @param page  corresponding page number to display 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws FileNotFoundException
	 */

	public void display(String book, int page) throws IOException, ClassNotFoundException, FileNotFoundException{
		
		currBookName = book;
		currPageNum = page;
		String readerUser = this.userName;

		// part 1 of handshaking 
		Socket socket = new Socket(serverAddress, serverPortNum);	

		ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream()); 
		output.writeObject(new Page(book, page));

		ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
		Page displayedPage = (Page) input.readObject();

		// if operating in pull mode, send a query to server for posts
		if (this.mode.equals("pull")) {
			updateForumPosts (book, page, readerUser);
		}
		
		updateLineStatus (displayedPage);

		// start timer 
		// not applicable for push mode 
		if (this.mode.equals("pull")) {
			pullTimer(book, page);
		}	
	}

	/**
	 * Method that executes post_to_forum <line_num> <content> 
	 * @param lineNum  line number that reader wants to post to
	 * @param content  message ( numerical, spaces, punctuation and letters all can be typed)
	 * @throws Exception
	 */
	public void post_to_forum(int lineNum, String content) throws Exception {
		// error checking. making sure that there is a book and page that can be posted on
		if (currBookName == null ) {
			System.out.println("Don't know where to post to");		
		}
		
		Socket socket = new Socket(serverAddress, serverPortNum);
		ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
	
		Post post = new Post(0, content, userName, currBookName, currPageNum, lineNum);
		output.writeObject(post);

	}

	/**
	 * Method that executes read_post <line_num> command  <br>
	 * Only prints UNREAD posts
	 * @param lineNum line number that you want to read posts from
	 * @throws Exception 
	 * @throws ClassNotFoundException 
	 */
	public void read_post(int lineNum) throws Exception {
		
		updateForumPosts(currBookName, currPageNum, "");
		System.out.println("Displaying all unread messages for page " + currPageNum + " of " + currBookName + ": line " + lineNum );
		for (Post p: listOfPosts) {
			// book title, page and line matches what the reader is on and NOT READ
			if (p.getBookName().equals(currBookName) && (p.getPageNum() == currPageNum) 
					&& (p.getLineNum() == lineNum) && (p.read == false) ) {
				// sets to read to prevent it from being read again
				p.setTrue();
				System.out.println(p.getSerialNum() + " " + p.getUserName() + " : " + p.getContent());
			}		
		}
	}

	/**
	 * Private helper method that checks whether a certain line has any unread posts or not
	 * @param displayedPage 
	 */
	private void updateLineStatus (Page displayedPage) {
		
		// lines start from line 1 as seen in e-book page files 
		int lineNum = 1;
		for (String text: displayedPage.lines) {
			String status = " ";
			// reset to nothing and checks all posts stored in reader
			for (Post p: listOfPosts) {
				// post must be from the page and book that the reader is on 
				if (p.getBookName().equals(currBookName) && (p.getPageNum() == (currPageNum)) 
						&& (p.getLineNum() == lineNum)){
					if (status != "n" ) {		
						if (p.read){
							status = "m";
							break;
						} else {
							status = "n";		
						}
					}
				}
			}
			// print out the line and checks the next line
			System.out.print(status);
			System.out.println(text);
			lineNum ++;
		}
	}

	/**
	 * Private helper method that updates the posts stored in the reader database
	 * @param book  name of book
	 * @param page  page number
	 * @param user  user of the reader 
	 * @return      the number of posts between the difference of posts between the server and the reader
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private int updateForumPosts(String book, int page, String user) throws IOException, ClassNotFoundException {

		int readerPosts = 0;
		int serverPosts = 0;
		int newPosts = 0;

		Socket socket = new Socket(serverAddress, serverPortNum);
		ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
		output.writeObject(new PostDataBase(book, page, user));

		ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
		PostDataBase curr = (PostDataBase) input.readObject();

		readerPosts = listOfPosts.size();
		serverPosts = curr.listOfPosts.size();
		
		// if readerPosts and serverPosts are equal, there's no new posts 
		if (readerPosts == serverPosts) {
			return newPosts;
		} else {
			// hashset addAll = only adds those not in the set
			listOfPosts.addAll(curr.listOfPosts);
		}

		newPosts = serverPosts - readerPosts; 
		socket.close();
		return newPosts;
	}

	/**
	 * Private function that sets the timer and queries the server when the timer expires
	 * @param book  book name
	 * @param page  page num
	 */
	private void pullTimer(String book, int page) {
		// reset it if there is a timer already since there's a new display command
		if (interval != null){
			interval.cancel();
		}

		final int pageNum = page;
		final String bookName = book;
		final String userName = this.userName;

		interval = new Timer();
		// schedules to update forum posts every pollingInterval milliseconds
		interval.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				try {
					int newPosts = updateForumPosts(bookName, pageNum, userName);
					
					if (newPosts > 0) {
						System.out.println("There are new posts.");
					}
				} catch (Exception e) { }
			}
		} , pollingInterval , pollingInterval);				
	}

	/**
	 * Private method that pushes posts to a new reader running on push mode <br>
	 * Creates a new thread which indicate whether there are new posts 
	 * @param portNum    port number that the socket is connect to
	 * @param userName   username of reader
	 * @throws Exception
	 */
	private void registerPushUser (int portNum, String userName) throws Exception {
		Socket socket = new Socket(serverAddress, serverPortNum);
		ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
		ArrayList <Integer> downloadedPosts = new ArrayList <Integer>();
		// add a database of ids of posts. each post is unique as each serial num is unique
		for (Post p: listOfPosts) {
			int id = p.getSerialNum(); 
			downloadedPosts.add(id);
		}
		
		output.writeObject(new PushUser(portNum, InetAddress.getLocalHost().getHostAddress(), userName, downloadedPosts));
		ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
		PostDataBase list = (PostDataBase) input.readObject();

		for (Post p: list.listOfPosts) {
			listOfPosts.add(p);
		}
		new Thread(new PushFunction(this)).start();
	}

	/**
	 * Private method that returns a number between 1024 and 65535 
	 * @return a number that can be used as a port number
	 */
	private int randomPortNumber() {		
		Random rand = new Random(); 
		int num = rand.nextInt((MAX- MIN) + 1) + MIN;
		return num;
	}

	private String mode;
	private int pollingInterval;
	private String userName;
	private String serverName;
	private int serverPortNum; 
	private InetAddress serverAddress;

	public String currBookName;
	public int currPageNum;
	ServerSocket pushSocket;
	Timer interval; 

	// for the AddAll for sets 
	// all other lists are ArrayList data structure
	HashSet <Post> listOfPosts = new HashSet<Post>();

	private static final int VALID_INPUT = 5;
	private static final int MODE = 0;
	private static final int POLLING_INTERVAL = 1;
	private static final int USER_NAME = 2;
	private static final int SERVER_NAME = 3;
	private static final int SERVER_PORT_NUM = 4;

	private static final int BOOK_NAME = 0;
	private static final int PAGE_NUM = 1;

	private static final int LINE_NUM = 0;
	private static final int CONTENT = 1;

	private static final int MAX = 65535;
	private static final int MIN = 1024;
}



/**
 * COMP3331 Ass 1 (E-reader + server) 
 * @author Annie Sun (z5075255)
 */

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class PushFunction implements Runnable {
	/**
	 * Creates a PushFunction object
	 * @param pushReader name of reader running in push mode
	 */
	public PushFunction (Reader pushReader) {
		this.reader = pushReader;
	}

	/** 
	 * @Override
	 * Responsible for informing whether they are new posts for push users 
	 * and adding new posts to reader's database
	 */
	public void run() {
		while(true) {
			try {
				Socket connectionSocket = reader.pushSocket.accept();
				ObjectInputStream input = new ObjectInputStream(connectionSocket.getInputStream());
				Post post = (Post) input.readObject();

				if (post.getBookName().equals(reader.currBookName) && 
						(post.getPageNum() == reader.currPageNum)){
					System.out.println("There are new posts.");
				}

				reader.listOfPosts.add(post);
				connectionSocket.close();

			} catch (IOException e) {
			} catch (ClassNotFoundException e) {
			}
		}
	}
	public Reader reader;
}

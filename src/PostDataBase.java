/**
 * COMP3331 Ass 1 (E-reader + server) 
 * @author Annie Sun (z5075255)
 */

import java.util.ArrayList;
import java.io.Serializable;

public class PostDataBase implements Serializable {

	/**
	 * Creates a serializable postdatabase object
	 * @param book name of the book
	 * @param page page number
	 * @param userName 
	 */
	public PostDataBase(String book, int page, String userName) {
		this.bookName = book;
		this.pageNum = page;
		this.userName = userName;	
	}

	/**
	 * Returns the book name as a string
	 * @return name of the book
	 */
	public String getBookName(){
		return this.bookName;
	}

	/**
	 * Returns the page number as an int
	 * @return page number
	 */
	public int getPageNum() {
		return this.pageNum;
	}

	/**
	 * Returns the user name as a string
	 * @return user name
	 */
	public String getUserName(){
		return this.userName;
	}

	private String bookName;
	private int pageNum; 
	private String userName;
	public ArrayList<Post> listOfPosts = new ArrayList<Post>(); 

	/** 
	 * @Override
	 * Converts PostDataBase object into a string representation
	 */
	public String toString() {
		return "PostDataBase{" + "bookName=" + bookName + ", pageNum=" + pageNum + "userName=" + userName +
				", listOfPosts=" + listOfPosts + '}';
	}
}

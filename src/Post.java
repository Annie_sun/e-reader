/**
 * COMP3331 Ass 1 (E-reader + server) 
 * @author Annie Sun (z5075255)
 */

import java.io.Serializable;

public class Post implements Serializable {

	/**
	 * Creates a Serializable Object
	 * @param serialNum unique serial num (also known as ID)
	 * @param content   the text or whatever the user wants to post on the forum
	 * @param userName  username of poster
	 * @param bookName  name of book that it is posted on
	 * @param pageNum   page number that it is posted on 
	 * @param lineNum   line that it is posted on
	 */
	public Post(int serialNum, String content, String userName, String bookName, int pageNum, int lineNum) {
	    this.serialNum = serialNum;
		this.content = content;
		this.userName = userName;
		this.bookName = bookName;
		this.pageNum = pageNum;
		this.lineNum = lineNum;
	}

	public boolean read = false;
	
	/**
	 * Method that sets a post to read status 
	 */
	public void setTrue() {
		read = true;
	}

	/**
	 * Method that sets an unique number that identifies the post
	 * @param num  unique number created by server to be assigned 
	 * as serial number for a post
	 */
	public void setSerialNum(int num) {
		this.serialNum = num;
	}
	
	/**
	 * Returns the book name as a string 
	 * @return book name 
	 */
	public String getBookName() {
		return this.bookName;
	}

	/**
	 * Returns the page number as an int
	 * @return page number
	 */
	public int getPageNum(){
		return this.pageNum;
	}

	/**
	 * Returns the content of the post as a string
	 * @return forum post message
	 */
	public String getContent() {
		return this.content;
	}

	/**
	 * Returns the user name as a string
	 * @return username of the user who posted the post
	 */
	public String getUserName(){
		return this.userName;
	}

	/**
	 * Returns the serial number as an int
	 * @return unique identifier for the post
	 */
	public int getSerialNum () {
		return this.serialNum;
	}

	/**
	 * Returns the line number as an int
	 * @return line number that post was posted on
	 */
	public int getLineNum(){
		return this.lineNum;
	}
	
	/**
	 * Provides a random hash function for the hash set 
	 * @return 
	 */

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 23 * hash + this.serialNum;
		hash = 23 * hash + this.content.hashCode();
		hash = 23 * hash + this.userName.hashCode();
		hash = 23 * hash + this.bookName.hashCode();
		hash = 23 * hash + this.pageNum;
		hash = 23 * hash + this.lineNum;
		return hash;
	}

	/**
	 * @Override 
	 * helper method that used to prevent duplicates of posts from appearing 
	 * <br>basically if one of the following fields isn't the same, there is no way
	 * it is the same post: <br>
	 * <i>class, serial num, content, userName, bookName, pageNum, lineNum </i>
	 * <br> or there is no such object
	 */

	public boolean equals(Object obj) {
		if (obj == null ) {
			return false;
		} else if (obj.getClass() != getClass()) {
			return false;
		}

		final Post newPost = (Post) obj;

		if (newPost.serialNum != this.serialNum) {
			return false;
		} else if (!newPost.content.equals(this.content)){
			return false;
		} else if (!newPost.userName.equals(this.userName)) {
			return false;
		} else if (!newPost.bookName.equals(this.bookName)){
			return false;
		} else if (newPost.pageNum != this.pageNum) {
			return false;
		} else if (newPost.lineNum != this.lineNum) {
			return false;
		}
		return true;	
	}

	/** 
	 * @Override
	 * Converts  Post object into a string representation
	 */
	public String toString() {
		return "Post{" + "serialNum=" + serialNum + ", content=" + content + ", userName=" + userName + ", bookName=" + bookName + ", pageNum=" + pageNum + ", lineNum=" + lineNum + ", read=" + read + '}';
	}
	
	private int serialNum;
	private String content;
	private String userName;
	private String bookName;
	private int pageNum;
	private int lineNum;
}

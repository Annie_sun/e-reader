/**
 * COMP3331 Ass 1 (E-reader + server) 
 * @author Annie Sun (z5075255)
 */

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Server {
	/**
	 * Main method of Server 
	 * Creates a Server object 
	 * @param args   only port number as input
	 * @throws Exception
	 */
	public static void main (String[] args) throws Exception {
		if (args.length == VALID_INPUT) {
			int portNum = Integer.parseInt(args[PORT_NUM]);
			Server s = new Server(portNum);
		}
	}

	
	public Server (int portNum) throws Exception {
		
		ServerSocket socket = new ServerSocket(portNum);
		
		// initialise a database of posts as well as list of readers operating in push mode 
		listOfPosts = new ArrayList<Post>();
		listOfPushReaders = new ArrayList<PushUser>();

		System.out.println("The server is listening on port number " + portNum );
		System.out.println("The database for discussion posts has been initialised");

		while (true) {
			// accepts the handshaking (part 2) 
			Socket connectionSocket = socket.accept();

			ObjectInputStream inFromReader = new ObjectInputStream(connectionSocket.getInputStream());
			ObjectOutputStream outToReader = new ObjectOutputStream(connectionSocket.getOutputStream());  
			
			forward(inFromReader, outToReader);
			connectionSocket.close();
		}	
	}

	/**
	 * Method responsible for sending the commands to the correct method 
	 * @param inFromReader  input from reader
	 * @param outToReader   output stream for the reader
	 * @throws Exception
	 */
	private void forward(ObjectInputStream inFromReader, ObjectOutputStream outToReader) throws Exception {
		Object line = inFromReader.readObject();
		Object command = line.getClass();

		if (command.equals(Page.class)){
			displayPage((Page) line, outToReader);		
		} else if (command.equals(PushUser.class)) {
			pushUserList((PushUser) line, outToReader);
		} else if (command.equals(Post.class)) {			
			newPost ((Post) line);		
		} else if (command.equals(PostDataBase.class)) {
			queryPostDataBase((PostDataBase) line, outToReader);
		}  else {	
			System.out.println("Unable to process");
		}
	}

	/**
	 * Method which deals with initializing readers in push mode <br>
	 * -Adds reader to list of push readers <br>
	 * -Prints out the user who requested push mode, and indicate that summary has been received from the reader <br>
	 * -Adds posts to reader database while printing corresponding serial numbers
	 * @param user     name of reader
	 * @param output   output stream
	 * @throws IOException
	 */
	private void pushUserList (PushUser user, ObjectOutputStream output) throws IOException {
		listOfPushReaders.add(user);
		System.out.println("Received push request from " + user.getPushUser() + " and has been added to push list");

		PostDataBase allPosts = new PostDataBase("", 0, user.getPushUser());
		allPosts.listOfPosts = new ArrayList<Post>();
		ArrayList<Post> forwardList = allPosts.listOfPosts;

		System.out.println( "Summary from "+ user.getPushUser() + "'s reader has been received");
		System.out.println("New posts have been forwarded. ");
		System.out.print("Posts [");

		for (Post p: listOfPosts) {
			if (!user.getPostIDs().contains(p.getSerialNum())) {
				forwardList.add(p);
				System.out.print(p.getSerialNum() + " ");		
			}
		}
		System.out.println("] have been forwarded ");
		output.writeObject(allPosts);
		output.close();
	}

	/**
	 * Method that sends a post to a push mode reader
	 * @param p     a post object 
	 * @param user  push reader user 
	 */
	private void pushPost (Post p, PushUser user) {
		try {
			System.out.println("Pushing messages to " + user.getPushUser() + ".");
			Socket socket = new Socket (user.getHostName(), user.getPortNum());
			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			out.writeObject(p);
		} catch (Exception e) {
		}
	}

	/**
	 * Method that deals with queries from reader
	 * -Prints out the specific user that the post is received from 
	 * @param db       post database 
	 * @param output   output stream
	 * @throws IOException
	 */
	private void queryPostDataBase(PostDataBase db, ObjectOutputStream output) throws IOException{
		
		if (!db.getUserName().equals("")) {
			System.out.println("Received query from " + db.getUserName() + ".");
		}
		
		for (Post p: listOfPosts) {
			if (p.getBookName().equals(db.getBookName()) && p.getPageNum() == (db.getPageNum())){
				db.listOfPosts.add(p);
			}
		}
		output.writeObject(db);
		output.close();
	}

	/**
	 * Method that adds a new post to the database <br>
	 * -Assigns unique serial number for the post  <br>
	 * -Prints that post has been added to database <br>
	 * -Send post to all push mode readers  <br>
	 * -Or else states that there is no push mode readers and therefore no need for action
	 * @param p  post 
	 */
	private void newPost(Post p) {

		// will always provide us with an unique number since we can't remove post
		int uniqueNum = listOfPosts.size();
		listOfPosts.add(p);
		
		System.out.println("New post received from " + p.getUserName());
		p.setSerialNum(uniqueNum);
		System.out.println("Post added to the database and given serial number (" + 
				p.getBookName() + ", " + p.getPageNum() + ", " + p.getLineNum() + ", " + 
				p.getSerialNum() + ").");

		for (PushUser pu: listOfPushReaders) {
			pushPost(p, pu);
		}

		if (listOfPushReaders.isEmpty()) {
			System.out.println("Push list empty. No action required");
		}
	}

	/**
	 * Method responsible for reading in the lines of a page <br>
	 * Must be in format of <bookName>_page<page_num> eg. joyce_page1
	 * @param curr         page wanted for display
	 * @param outToReader  output stream 
	 * @throws IOException
	 */
	private void displayPage(Page curr, ObjectOutputStream outToReader) throws IOException {

		String fileName;
		fileName = curr.getBookName() + "_page" + curr.getPageNum();
		// opens the file and adds the lines to the page and writes to output
		Scanner s = new Scanner(new File(fileName));
		ArrayList<String> lines = new ArrayList<String>();
		
		while (s.hasNextLine()) {
			lines.add(s.nextLine());	
		}	
		curr.lines = lines;
		outToReader.writeObject(curr);
		outToReader.close();
		s.close();
	}

	private static final int VALID_INPUT = 1;
	private static final int PORT_NUM = 0;

	public ArrayList<Post> listOfPosts;
	private ArrayList<PushUser> listOfPushReaders; 

}

